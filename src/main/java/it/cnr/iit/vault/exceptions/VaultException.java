package it.cnr.iit.vault.exceptions;

public class VaultException extends Exception {

	private static final long serialVersionUID = 1L;

	public VaultException(String message) {
		super(message);
	}
}
