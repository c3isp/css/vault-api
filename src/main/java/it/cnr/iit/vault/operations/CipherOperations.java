package it.cnr.iit.vault.operations;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.UUID;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;
import java.util.zip.Checksum;

import javax.crypto.Cipher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.vault.VaultException;

import com.google.common.hash.Hashing;

import it.cnr.iit.vault.types.VaultDatakey;
import it.cnr.iit.vault.types.VaultWrapper;

@Component
public class CipherOperations {

	@Autowired
	private VaultWrapper vaultWrapper;

	public static final String ENCRYPT_MODE = "encrypt";
	public static final String DECRYPT_MODE = "decrypt";
	public static final int BLOCK_SIZE = 1024 * 1024;

	public String performOperation(VaultDatakey datakey, String uriInput, String mode) {
		try {

			byte[] nonce = null;
			Cipher cipher = null;
			String filename = UUID.randomUUID().toString();

			if (mode.equals(ENCRYPT_MODE)) {
				nonce = vaultWrapper.getOperationsUtils().generateRandomNonce();
				filename += ".enc";
				cipher = vaultWrapper.getOperationsUtils().initCipher(datakey, nonce, ENCRYPT_MODE);
			} else if (mode.equals(DECRYPT_MODE)) {
				nonce = vaultWrapper.getOperationsUtils().getNonceFromEncryptedFile(uriInput);
				filename += ".dec";
				cipher = vaultWrapper.getOperationsUtils().initCipher(datakey, nonce, DECRYPT_MODE);
			} else {
				throw new VaultException("Invalid mode operation. Allowed are \"encrypt\" and \"decrypt\"");
			}

			String uriOutput = "/tmp/" + filename;
			FileInputStream fis = new FileInputStream(new File(uriInput));
			FileOutputStream fos = new FileOutputStream(new File(uriOutput));

			if (mode.equals(ENCRYPT_MODE)) {
				fos.write(nonce);
			}
			if (mode.equals(DECRYPT_MODE)) {
				fis.skip(12);
			}

			doComputation(fis, fos, cipher);

			fos.flush();
			fis.close();
			fos.close();

			return uriOutput;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	private void doComputation(FileInputStream fis, FileOutputStream fos, Cipher cipher) {
		try {
			int nread = 0;
			byte[] inbuf = new byte[BLOCK_SIZE];

			long endTime = 0;
			long startTime = System.currentTimeMillis();

			while ((nread = fis.read(inbuf)) > 0) {
				byte[] trimbuf = new byte[nread];
				for (int i = 0; i < nread; i++) {
					trimbuf[i] = inbuf[i];
				}

				byte[] tmp = cipher.update(trimbuf);

				if (tmp != null) {
					fos.write(tmp);
				}
			}

			endTime = System.currentTimeMillis() - startTime;

			byte[] computedText = cipher.doFinal();
			if (computedText != null) {
				fos.write(computedText);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public long getChecksumCRC32BigFiles(InputStream stream) throws IOException {
		CheckedInputStream checkedInputStream = new CheckedInputStream(stream, new CRC32());
		byte[] buffer = new byte[BLOCK_SIZE];
		while (checkedInputStream.read(buffer, 0, buffer.length) >= 0) {
		}
		return checkedInputStream.getChecksum().getValue();
	}

	public long getChecksumCRC32String(byte[] bytes) {
		Checksum crc32 = new CRC32();
		crc32.update(bytes, 0, bytes.length);
		return crc32.getValue();
	}

	public String getSha256(String s) {
		return Hashing.sha256().hashString(s, StandardCharsets.UTF_8).toString();
	}

}
