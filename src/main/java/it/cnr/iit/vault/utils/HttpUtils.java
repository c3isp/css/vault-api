package it.cnr.iit.vault.utils;

import java.net.URI;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cnr.iit.vault.types.VaultCiphertext;

@Component
public class HttpUtils {

	@Value("${vault.api.endpoint}")
	private String vaultEndpoint;
	@Value("${vault.api.getDatakey}")
	private String getDatakey;
	@Value("${vault.api.decryptDatakey}")
	private String decryptDatakey;
	@Value("${vault.token}")
	private String token;

	public HttpUtils() {
	}

	public String getCompleteDataKeyResponse() {
		URI uri = UriComponentsBuilder.fromUriString(vaultEndpoint).path(getDatakey).build().toUri();
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.set("X-Vault-Token", token);
		HttpEntity<?> entity = null;
		entity = new HttpEntity<>(null, requestHeaders);
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.postForObject(uri, entity, String.class);
	}

	public String decryptDatakey(VaultCiphertext vaultCiphertext) throws JsonProcessingException {
		URI uri = UriComponentsBuilder.fromUriString(vaultEndpoint).path(decryptDatakey).build().toUri();
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.set("X-Vault-Token", token);
		HttpEntity<?> entity = null;
		entity = new HttpEntity<>(new ObjectMapper().writeValueAsBytes(vaultCiphertext), requestHeaders);
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.postForObject(uri, entity, String.class);
	}

}
