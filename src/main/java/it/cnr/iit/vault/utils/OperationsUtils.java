package it.cnr.iit.vault.utils;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.stereotype.Component;

import it.cnr.iit.vault.operations.CipherOperations;
import it.cnr.iit.vault.types.VaultDatakey;

@Component
public class OperationsUtils {

	public static final int NONCE_SIZE = 12;
	public static final String ALGORITHM = "AES";
	public static final String CIPHER_TRANSFORMATION = "AES/GCM/PKCS5Padding";

	public OperationsUtils() {
	}

	public byte[] getNonceFromEncryptedFile(String uriInput) {
		try {
			File f = new File(uriInput);
			byte[] content = Files.readAllBytes(Paths.get(f.getPath()));
			byte[] nonce = Arrays.copyOfRange(content, 0, NONCE_SIZE);

			return nonce;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public byte[] generateRandomNonce() {
		byte[] nonce = new byte[NONCE_SIZE];
		SecureRandom secureRandom = new SecureRandom();
		secureRandom.nextBytes(nonce);
		return nonce;
	}

	public Cipher initCipher(VaultDatakey datakey, byte[] nonce, String mode) {

		byte[] decodedKey = Base64.getDecoder().decode(datakey.getPlaintext().getBytes());

		SecretKeySpec secretKeySpec = new SecretKeySpec(decodedKey, ALGORITHM);
		GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(16 * 8, nonce);

		try {
			Cipher cipher = Cipher.getInstance(CIPHER_TRANSFORMATION);
			if (mode.equals(CipherOperations.ENCRYPT_MODE)) {
				cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, gcmParameterSpec);
			}
			if (mode.equals(CipherOperations.DECRYPT_MODE)) {
				cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, gcmParameterSpec);
			}
			return cipher;
		} catch (Exception e) {
			System.out.println("Bad configuration parameters for the Cipher in encryption");
			e.printStackTrace();
		}

		return null;
	}
}
