package it.cnr.iit.vault.types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.cnr.iit.vault.operations.CipherOperations;
import it.cnr.iit.vault.utils.HttpUtils;
import it.cnr.iit.vault.utils.OperationsUtils;

@Component
public class VaultWrapper {

	@Autowired
	private OperationsUtils operationsUtils;

	@Autowired
	private CipherOperations cipherOperations;

	@Autowired
	private HttpUtils httpUtils;

	public VaultWrapper() {
	}

	public OperationsUtils getOperationsUtils() {
		return operationsUtils;
	}

	public void setOperationsUtils(OperationsUtils operationsUtils) {
		this.operationsUtils = operationsUtils;
	}

	public CipherOperations getCipherOperations() {
		return cipherOperations;
	}

	public void setCipherOperations(CipherOperations cipherOperations) {
		this.cipherOperations = cipherOperations;
	}

	public HttpUtils getHttpUtils() {
		return httpUtils;
	}

	public void setHttpUtils(HttpUtils httpUtils) {
		this.httpUtils = httpUtils;
	}

}
