package it.cnr.iit.vault.types;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VaultCiphertext {

	@JsonProperty("ciphertext")
	private String ciphertext;

	public VaultCiphertext() {
	}

	public String getCiphertext() {
		return ciphertext;
	}

	public void setCiphertext(String ciphertext) {
		this.ciphertext = ciphertext;
	}

}
