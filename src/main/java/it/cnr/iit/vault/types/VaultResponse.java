package it.cnr.iit.vault.types;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VaultResponse {
	@JsonProperty("request_id")
	private String requestId;
	@JsonProperty("lease_id")
	private String leaseId;
	@JsonProperty("renewable")
	private boolean renewable;
	@JsonProperty("lease_duration")
	private int leaseDuration;
	@JsonProperty("data")
	private VaultDatakey datakey;
	@JsonProperty("wrap_info")
	private String wrapInfo;
	@JsonProperty("warnings")
	private String warnings;
	@JsonProperty("auth")
	private String auth;

	public VaultResponse() {
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getLeaseId() {
		return leaseId;
	}

	public void setLeaseId(String leaseId) {
		this.leaseId = leaseId;
	}

	public boolean isRenewable() {
		return renewable;
	}

	public void setRenewable(boolean renewable) {
		this.renewable = renewable;
	}

	public int getLeaseDuration() {
		return leaseDuration;
	}

	public void setLeaseDuration(int leaseDuration) {
		this.leaseDuration = leaseDuration;
	}

	public VaultDatakey getDatakey() {
		return datakey;
	}

	public void setDatakey(VaultDatakey datakey) {
		this.datakey = datakey;
	}

	public String getWrapInfo() {
		return wrapInfo;
	}

	public void setWrapInfo(String wrapInfo) {
		this.wrapInfo = wrapInfo;
	}

	public String getWarnings() {
		return warnings;
	}

	public void setWarnings(String warnings) {
		this.warnings = warnings;
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

}
