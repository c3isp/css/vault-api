package it.cnr.iit.vault.types;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VaultDatakey {
	@JsonProperty("ciphertext")
	private String ciphertext;
	@JsonProperty("plaintext")
	private String plaintext;

	public VaultDatakey() {
	}

	public String getCiphertext() {
		return ciphertext;
	}

	public void setCiphertext(String ciphertext) {
		this.ciphertext = ciphertext;
	}

	public String getPlaintext() {
		return plaintext;
	}

	public void setPlaintext(String plaintext) {
		this.plaintext = plaintext;
	}

}
