package it.cnr.iit.vault.deployer;

import java.net.URI;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.vault.authentication.ClientAuthentication;
import org.springframework.vault.authentication.TokenAuthentication;
import org.springframework.vault.client.VaultEndpoint;
import org.springframework.vault.config.AbstractVaultConfiguration;

@Configuration
public class VaultConfig extends AbstractVaultConfiguration {

	@Value("${vault.api.endpoint}")
	private String vaultEndpoint;
	@Value("${vault.token}")
	private String vaultToken;

	@Override
	public ClientAuthentication clientAuthentication() {
		return new TokenAuthentication(vaultToken);
	}

	@Override
	public VaultEndpoint vaultEndpoint() {
		return VaultEndpoint.from(URI.create(vaultEndpoint));
	}
}