package it.cnr.iit.vault.deployer;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import it.cnr.iit.vault.operations.CipherOperations;
import it.cnr.iit.vault.utils.HttpUtils;

@SpringBootApplication
@ComponentScan("it.cnr.iit")
public class VaultDeployer implements CommandLineRunner {

	private final static Logger log = Logger.getLogger(VaultDeployer.class.getName());

	@Autowired
	private HttpUtils httpUtils;
	@Autowired
	private CipherOperations cipherOperations;

	public static void main(String[] args) {
		SpringApplication.run(VaultDeployer.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
//		try {
//			
//			String response = httpUtils.getCompleteDataKeyResponse();
//			VaultResponse vaultResponse = new ObjectMapper().readValue(response, VaultResponse.class);
//			VaultDatakey datakey = vaultResponse.getDatakey();
//
//			System.out.println("datakey ciphertext: " + datakey.getCiphertext());
//			System.out.println("datakey plaintext: " + datakey.getPlaintext());
//
//			String sha256hex = cipherOperations.getSha256(datakey.getPlaintext());
//
//			String inputFile = System.getProperty("user.home") + "/Desktop/prova";
//
//			String cipheredFileUri = cipherOperations.performOperation(datakey, inputFile,
//					CipherOperations.ENCRYPT_MODE);
//
//			VaultCiphertext vaultCiphertext = new VaultCiphertext();
//			vaultCiphertext.setCiphertext(datakey.getCiphertext());
//			response = httpUtils.decryptDatakey(vaultCiphertext);
//			vaultResponse = new ObjectMapper().readValue(response, VaultResponse.class);
//
//			String plainFileUri = cipherOperations.performOperation(vaultResponse.getDatakey(), cipheredFileUri,
//					CipherOperations.DECRYPT_MODE);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
	}
}
